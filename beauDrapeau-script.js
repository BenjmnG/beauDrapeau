import { beauDrapeau } from 'beauDrapeau.js';

export default function () {

  // Declare selector
  let toggleInput = pagedjs_gui.ui.beauDrapeau.toggleInput

  // Add event
  toggleInput.addEventListener('input', () => {
    
    // Change toggle button style and content
    //beauDrapeau().ui().toggle().button('edit-button')
    
    // Run function
    beauDrapeau().edit()
  })
}